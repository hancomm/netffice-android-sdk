package com.netffice.example;

import android.webkit.MimeTypeMap;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by accent on 15. 2. 26.
 */
class StupidExt2Mime {
    private static HashMap<String, String> mimeTypeMap = new HashMap<String, String>();

    static {
        mimeTypeMap.put("txt", "text/plain");
        mimeTypeMap.put("xml", "text/xml");
        mimeTypeMap.put("rtf", "application/rtf");
        mimeTypeMap.put("doc", "application/vnd.ms-word");
        mimeTypeMap.put("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        mimeTypeMap.put("hwdt", "application/hancomhwdt");
        mimeTypeMap.put("xls", "application/vnd.ms-excel");
        mimeTypeMap.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        mimeTypeMap.put("xlsm", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        mimeTypeMap.put("csv", "text/comma-separated-values");
        mimeTypeMap.put("ppt", "application/vnd.ms-powerpoint");
        mimeTypeMap.put("pps", "application/vnd.ms-powerpoint");
        mimeTypeMap.put("pot", "application/vnd.ms-powerpoint");
        mimeTypeMap.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
        mimeTypeMap.put("ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow");
        mimeTypeMap.put("potx", "application/vnd.openxmlformats-officedocument.presentationml.template");
        mimeTypeMap.put("hwp", "application/hwp");
        mimeTypeMap.put("hwt", "application/hwt");

        // others
        mimeTypeMap.put("htm", "text/html");
        mimeTypeMap.put("html", "text/html");

        // media
        mimeTypeMap.put("aac", "audio/aac");
        mimeTypeMap.put("m4a", "audio/mp4");
        mimeTypeMap.put("m4v", "video/mp4");
        mimeTypeMap.put("3gpp", "video/3gpp");
        mimeTypeMap.put("3g2", "video/3gpp2");
        mimeTypeMap.put("3gpp2", "video/3gpp2");
        mimeTypeMap.put("avi", "video/avi");
        mimeTypeMap.put("divx", "video/divx");
        mimeTypeMap.put("oga", "application/ogg");
        mimeTypeMap.put("flac", "audio/flac");
        mimeTypeMap.put("swf", "application/x-shockwave-flash");
    }

    private StupidExt2Mime() {
        // do not create an instance
    }

    /**
     * @param ext dot을 제외한 확장자 ('docx')
     * @return
     */
    public static String getTypeFor(String ext) {
        if (ext == null) {
            return "";
        }
        ext = ext.toLowerCase(Locale.US);
        String value = mimeTypeMap.get(ext);
        if (value == null || value.equalsIgnoreCase("")) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(ext);
        }
        return value;
    }

    public static String getTypeFor(File file) {
        if(file==null) return null;
        String name = file.getName();
        int dotPos = name.lastIndexOf('.');
        String ext = (dotPos > 0) ? name.substring(dotPos + 1) : null;
        return getTypeFor(ext);
    }
}