package com.netffice.example;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import com.netffice.clientlib.Callback;
import com.netffice.clientlib.IntentReader;
import com.netffice.clientlib.NetfficeConnect;

import java.io.File;
import java.util.List;

import static com.netffice.clientlib.NetfficeConnect.PACKAGE_HANCOM_OFFICE_ANDROID_PHONE;
import static com.netffice.clientlib.NetfficeConnect.PRODUCT_MOBILE;


public class ExampleActivity extends Activity implements Callback {
    public static final String TAG = "ExampleActivity";
    private static final int REQ_SAVE_AS = 8080;
    private static final int REQ_SIGN_IN = 8081;
    private static final int REQ_INSTALL_NETFFICE = 8082;
    private static final int REQ_OPEN_FILE = 8083;

    private Button btnAuthProduct = null;
    private NetfficeConnect netfficeConnect = null;
    private Drawable actionBarBackgroundNormal;
    private Button btnSaveAsDocx;
    private Switch swShowLocals;
    private Switch swShowClouds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        actionBarBackgroundNormal = getActionBarBackground(this);
        btnAuthProduct = (Button) findViewById(R.id.btn_auth_product);
        btnAuthProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 제품 인증 요청
                netfficeConnect.requestProductAuth();
            }
        });
        btnSaveAsDocx = (Button) findViewById(R.id.btn_save_as_docx);
        btnSaveAsDocx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = NetfficeConnect.createSaveAsIntent("NetfficeGuide", "docx", null);
                startActivityForResult(intent, REQ_SAVE_AS);
            }
        });

        Button btnSaveAsDocDocx = (Button) findViewById(R.id.btn_save_as_doc_docx);
        btnSaveAsDocDocx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] extsToShow = { "doc", "docx" };
                Intent intent = NetfficeConnect.createSaveAsIntent("NetfficeGuide", extsToShow, 1, null);
                startActivityForResult(intent, REQ_SAVE_AS);
            }
        });

        Button btnOpen = (Button) findViewById(R.id.btn_open);
        btnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean showLocals = swShowLocals.isChecked();
                boolean showClouds = swShowClouds.isChecked();
                Intent intent = NetfficeConnect.createFilePickerIntent(null, showLocals, showClouds);
                startActivityForResult(intent, REQ_OPEN_FILE);
            }
        });
        Button btnOpenWithInitDir = (Button) findViewById(R.id.btn_open_with_init_dir);
        btnOpenWithInitDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean showLocals = swShowLocals.isChecked();
                boolean showClouds = swShowClouds.isChecked();
                File dir = Environment.getExternalStorageDirectory();
                Intent intent = NetfficeConnect.createFilePickerIntent(dir, showLocals, showClouds);
                startActivityForResult(intent, REQ_OPEN_FILE);
            }
        });
        Button btnOpenDocDocx = (Button) findViewById(R.id.btn_open_doc_docx);
        btnOpenDocDocx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean showLocals = swShowLocals.isChecked();
                boolean showClouds = swShowClouds.isChecked();
                String[] extsToShow = { "doc", "docx" };
                Intent intent = NetfficeConnect.createFilePickerIntent(null, showLocals, showClouds, extsToShow);
                startActivityForResult(intent, REQ_OPEN_FILE);
            }
        });
        Button btnOpenPptPptx = (Button) findViewById(R.id.btn_open_ppt_pptx);
        btnOpenPptPptx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean showLocals = swShowLocals.isChecked();
                boolean showClouds = swShowClouds.isChecked();
                String[] extsToShow = { "ppt", "pptx" };
                Intent intent = NetfficeConnect.createFilePickerIntent(null, showLocals, showClouds, extsToShow);
                startActivityForResult(intent, REQ_OPEN_FILE);
            }
        });
        Button btnOpenXlsXlsx = (Button) findViewById(R.id.btn_open_xls_xlsx);
        btnOpenXlsXlsx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean showLocals = swShowLocals.isChecked();
                boolean showClouds = swShowClouds.isChecked();
                String[] extsToShow = { "xls", "xlsx" };
                Intent intent = NetfficeConnect.createFilePickerIntent(null, showLocals, showClouds, extsToShow);
                startActivityForResult(intent, REQ_OPEN_FILE);
            }
        });
        swShowLocals = (Switch) findViewById(R.id.switch_show_locals);
        swShowClouds = (Switch) findViewById(R.id.switch_show_clouds);
        Button btnShowProUpgrade = (Button) findViewById(R.id.btn_pro_upgrade);
        btnShowProUpgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = NetfficeConnect.createProGuideIntent();
                startActivity(intent);
            }
        });

        NetfficeConnect.DEBUG = true;
    }

    private static Drawable getActionBarBackground(Activity activity) {
        // memorize Actionbar background
        // http://stackoverflow.com/a/22872870/111890
        final int actionBarId = activity.getResources().getIdentifier("action_bar", "id", "android");
        final View actionBar = activity.findViewById(actionBarId);
        return actionBar.getBackground();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(NetfficeConnect.getPackageId()==null) {
            List<String> drivePkgs = NetfficeConnect.queryDrivePackages(getPackageManager());
            int size = drivePkgs.size();
            if(size==0) {   // 설치된 Drive app이 없는 경우
                Toast.makeText(this, "No installed Drive apps", Toast.LENGTH_LONG).show();
            } else {
                Intent signIntent = NetfficeConnect.createSignInIntent();
                startActivityForResult(signIntent, REQ_SIGN_IN);
            }
        } else {
            netfficeConnect = NetfficeConnect.connect(this, PRODUCT_MOBILE, PACKAGE_HANCOM_OFFICE_ANDROID_PHONE, "0.1-alpha", this);
            if(netfficeConnect==null) {  // 연결실패 (packageId가 잘못됬다던지??)
                Toast.makeText(this, "Failed to connect to "+NetfficeConnect.getPackageId(), Toast.LENGTH_LONG).show();
            } else {    // 연결 성공
                getActionBar().setSubtitle(NetfficeConnect.getPackageId());
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(netfficeConnect!=null) {
            netfficeConnect.disconnect();
            NetfficeConnect.setPackageId(null);
        }
    }

    @Override
    public void onSessionID(String sessionId) {
        if(sessionId==null) {    // not logged in
            Toast.makeText(this, TAG+": received session ID : Not logged in", Toast.LENGTH_SHORT).show();
            getActionBar().setBackgroundDrawable(actionBarBackgroundNormal);

            btnAuthProduct.setVisibility(View.GONE);

            // Sign-in 하시오
            startActivityForResult(NetfficeConnect.createSignInIntent(), REQ_SIGN_IN);
        } else {    // logged in
            Toast.makeText(this, TAG+": received session ID : "+sessionId, Toast.LENGTH_SHORT).show();

            // change actionbar color
            getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.ab_background_connected)));

            // 인증버튼 켜기 (실제 앱에서는 버튼없이 바로진행하는게 좋을듯
            btnAuthProduct.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onProductAuth(int remainingDevices ,int remainingDays, int permissionCode) {
        if(remainingDevices>0 && remainingDays>0) btnAuthProduct.setVisibility(View.GONE);

        // 여기선 동작확인을 위해 잔여일수에 상관없이 무조건 nagging을 띄운다.
        // 실제 앱에선 스펙(잔여일 3일이하면 nagging)에 맞게 조건처리 해야함.
        netfficeConnect.showNaggingByDays(remainingDays);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==Activity.RESULT_OK) {
            if(requestCode==REQ_SAVE_AS) {
                String resultPath = IntentReader.readResultPathExtra(data);
                NetfficeConnect.notifySaveCompleted(this, resultPath);
                Toast.makeText(this, String.valueOf(data.getExtras()), Toast.LENGTH_LONG).show();
            } else if(requestCode==REQ_OPEN_FILE) {
                String resultPath = IntentReader.readResultPathForPicker(data);
                File f = new File(resultPath);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String mime = StupidExt2Mime.getTypeFor(f);
                intent.setDataAndType(Uri.fromFile(f), mime);
                startActivity(intent);
            } else if(requestCode==REQ_SIGN_IN) {
                ComponentName compInfo = data.getComponent();
                NetfficeConnect.setPackageId(compInfo.getPackageName());
            }
        } else if(resultCode==Activity.RESULT_CANCELED) {
            if(requestCode==REQ_SAVE_AS) {
                Toast.makeText(this, "Save As Cancelled", Toast.LENGTH_LONG).show();
            } else if(requestCode==REQ_SIGN_IN) {
                // Sign-in을 취소하면 app도 종료시킨다
                finish();
            } else if(requestCode==REQ_INSTALL_NETFFICE) {
                // Netffice app 설치를 취소하면 app도 종료시킨다
                finish();
            }
        }
    }
}