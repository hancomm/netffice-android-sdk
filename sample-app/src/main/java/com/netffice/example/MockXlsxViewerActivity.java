package com.netffice.example;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;

import com.netffice.clientlib.IntentReader;

/**
 * 어느 drive app에서 호출된건지 알아내는 예제
 * Created by accent on 2016. 5. 13..
 */
public class MockXlsxViewerActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar ab = getActionBar();
        ab.setTitle("Called from");
        ab.setSubtitle(IntentReader.readDriveApp(getIntent()));
    }
}
