package com.netffice.clientlib;

/**
 * NetfficeConnect's callback
 * Created by accent on 15. 2. 14.
 */
public interface Callback {
    /** Netffice App으로 부터 Session ID를 받거나(Sign-in) null이면 Sign-out한 경우. 넷피스 유료 잔여기간이 필요하면 NetfficeConnect.requestProductAuth() 호출 */
    void onSessionID(String sessionId);
    /** 제품인증 결과 수신 */
    void onProductAuth(int remainingDevices ,int remainingDays, int permission);
}