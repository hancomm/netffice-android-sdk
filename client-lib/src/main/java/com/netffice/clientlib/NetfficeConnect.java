package com.netffice.clientlib;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by accent on 15. 2. 11.
 */
public class NetfficeConnect {
    // MSG_WHAT 상수들은 Netffice app의 NetfficeService에 정의된 값과 일치해야 함
    public static final int MSG_WHAT_GET_SESSION_ID = 2;
    public static final int MSG_WHAT_AUTH_PRODUCT = 3;
    public static final int MSG_WHAT_SHOW_NAGGING = 4;

    /** Sign-in activity를 띄우기 위한 action. */
    public static final String ACTION_SIGN_IN = "com.netffice24.intent.action.SIGN_IN";
    /** 초기 디렉토리 */
    public static final String EXTRA_INIT_DIR = "init_dir";
    public static final String EXTRA_SHOW_LOCALS = "com.netffice.intent.extra.SHOW_LOCALS";
    public static final String EXTRA_SHOW_CLOUDS = "com.netffice.intent.extra.SHOW_CLOUDS";
    public static final String EXTRA_INCLUDE_EXT_LIST = "com.netffice.intent.extra.INCLUDE_EXT_LIST";
    /** EXTRA_INCLUDE_EXT_LIST와 같이쓰면 좋겠지만 API역사가 이렇게 만들었다 SAVE에서만 EXTENSIONS를 사용하시오 */
    public static final String EXTRA_EXTENSIONS = "com.netffice24.intent.extra.EXTENSIONS";
    public static final String EXTRA_SAVEAS_SELECTED_EXT_INDEX = "com.netffice24.intent.extra.SELECTED_EXT_IDX";
    public static final String EXTRA_UNDO = "com.netffice.intent.extra.UNDO";

    public static final String KEY_PRODUCT_CODE = "productCode";
    public static final String KEY_PACKAGE_CODE = "packageCode";
    public static final String KEY_PACKAGE_VERSION = "packageVersion";


    public static final String KEY_RETURN_VALUE = "return";

    public static final String PKG_ID_NETFFICE_DRIVE = "com.netffice.drive";
    public static final String PRODUCT_DESKTOP = "01";
    public static final String PRODUCT_MOBILE = "02";
    public static final String PACKAGE_HANCOM_OFFICE_WIN = "1";
    public static final String PACKAGE_HANCOM_WORD_MAC = "2";
    public static final String PACKAGE_EZPHOTO = "3";
    public static final String PACKAGE_HANCOM_OFFICE_ANDROID_TABLET = "1";
    public static final String PACKAGE_HANCOM_WORD_ANDROID_PHONE = "2";
    public static final String PACKAGE_HANCOM_OFFICE_ANDROID_PHONE = "3";
    public static final String PACKAGE_HANCOM_OFFICE_IOS = "4";

    private static final String TAG = "NetfficeConnect";

    private String productCode;
    private String packageType;
    private String packageVersion;
    private ServiceConnection serviceConnection;
    private Messenger service;
    private boolean bound = false;
    private Callback callback;
    private Context cxt;
    private Messenger inMessenger;
    private BroadcastReceiver signoutReceiver;
    private static String packageId = null;
    /** DEBUG log를 남길지 여부 */
    public static boolean DEBUG = false;

    private NetfficeConnect(final Context cxt, final String productCode, final String packageType, final String packageVersion, final Callback callback) {
        this.cxt = cxt;
        this.productCode = productCode;
        this.packageType = packageType;
        this.packageVersion = packageVersion;
        if(callback==null) throw new IllegalArgumentException("Callback can not be null");
        this.callback = callback;
    }

    /**
     * 최종 setPackageId()의 결과를 반환. null가능
     * @return
     */
    public static String getPackageId() {
        return NetfficeConnect.packageId;
    }

    /** NetfficeConnect.connect()전에 호출되어야 함 */
    public static void setPackageId(String pkgId) {
        NetfficeConnect.packageId = pkgId;
        if(DEBUG) {
            Log.d(TAG, "setPackageId("+pkgId+")");
        }
    }

    private boolean bind(String drivePkgId) {
        // http://developer.android.com/guide/components/bound-services.html
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                if(DEBUG) {
                    Log.d(TAG, "service connected : "+componentName);
                }
                bound = true;
                service = new Messenger(iBinder);
                // for incoming messages
                inMessenger = new Messenger(new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        handleIncomingMsg(msg);
                    }
                });

                installSignoutReceiver();
                // to ensure Signed-in
                requestSessionID();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                if(DEBUG) {
                    Log.d(TAG, "service disconnected : "+componentName);
                }
                bound = false;
                service = null;
                inMessenger = null;

                cxt.unregisterReceiver(signoutReceiver);

                // Netffice App을 삭제한 경우
                callback.onSessionID(null);
            }
        };

        // http://stackoverflow.com/a/28054952/111890
        Intent intent = new Intent();
        intent.setClassName(drivePkgId, "com.tf.thinkdroid.drive.service.NetfficeService");
        if(DEBUG) {
            Log.d(TAG, "Trying to bind with "+intent);
        }
        return cxt.bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE);
    }

    /** connect(Context, String, String, Callback)의 반대 */
    public void disconnect() {
        if(bound) {
            cxt.unbindService(serviceConnection);
            bound = false;
        }
    }

    private void installSignoutReceiver() {
        IntentFilter filter = new IntentFilter("com.netffice.intent.action.broadcast.SIGNOUT");
        signoutReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // sign out
                callback.onSessionID(null);
            }
        };
        cxt.registerReceiver(signoutReceiver, filter);
    }

    private void handleIncomingMsg(Message msg) {
        switch (msg.what) {
            // return value of a GET_SESSION_ID request
            case MSG_WHAT_GET_SESSION_ID:
                Bundle data = msg.getData();
                String sessionId = data.getString(KEY_RETURN_VALUE);
                callback.onSessionID(sessionId);
                break;
            case MSG_WHAT_AUTH_PRODUCT:
                data = msg.getData();
                String jsonString = data.getString(KEY_RETURN_VALUE);
                try {
                    JSONObject json = new JSONObject(jsonString);
                    int devices = json.getInt("available_slots");
                    int days = json.getInt("remaining_days");
                    // offline cache된 결과를 받는 경우가 있으니 lastModified를 통한 보정 필요
                    long current = System.currentTimeMillis();
                    long lastModified = json.optLong("__LAST_MODIFIED__", current);
                    int modifiedDays = longToDays(lastModified);
                    int nowDays = longToDays(current);
                    // 보정 (어제 30일 남았으면 오늘은 29일 남은거임
                    days = days - (nowDays - modifiedDays);
                    int permissionCode = json.optInt("permission", 0);
                    callback.onProductAuth(devices, days, permissionCode);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                break;
            default :
                throw new IllegalArgumentException("Unknown message ID : "+msg.what);
        }
    }

    /**
     * 제품권한이 따로 필요한 앱인경우 session ID를 받았을때 호출 하시오
     */
    public final void requestProductAuth() {
        if(bound) {
            Message msg = Message.obtain(null, NetfficeConnect.MSG_WHAT_AUTH_PRODUCT);
            Bundle data = new Bundle();
            data.putString(KEY_PRODUCT_CODE, productCode);
            data.putString(KEY_PACKAGE_CODE, packageType);
            data.putString(KEY_PACKAGE_VERSION, packageVersion);
            msg.setData(data);
            msg.replyTo = inMessenger;
            try {
                service.send(msg);
            } catch (RemoteException e) {
                Log.i(TAG, "failed to authenticate this device", e);
            }
        } else {
            Log.i(TAG, "not bound");
        }
    }

    /** '넷피스 사용이 3일 남았습니다' 같은 Nagging을 띄우도록 요청한다. */
    public final void showNaggingByDays(int remainingDays) {
        if(bound) {
            Message msg = Message.obtain(null, NetfficeConnect.MSG_WHAT_SHOW_NAGGING);
            msg.arg1 = remainingDays;
            try {
                service.send(msg);
            } catch (RemoteException e) {
                Log.i(TAG, "showNaggingByDays failed", e);
            }
        } else {
            Log.i(TAG, "not bound");
        }
    }

    /**
     * 모든 동작의 시작은 여기서.
     * @param callback 3rd party app에서 구현해야 할 내용
     * @return null if Netffice app is not installed
     */
    public static NetfficeConnect connect(Context cxt, String productCode, String packageType, String packageVersion, Callback callback) {
        NetfficeConnect nc = new NetfficeConnect(cxt, productCode, packageType, packageVersion, callback);
        boolean success = nc.bind(NetfficeConnect.getPackageId());
        return success ? nc : null;
    }

    /** Netffice App 설치 페이지로 보내는 Intent를 생성한다 */
    public static Intent createNetfficeInstallIntent() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id="+getPackageId()));
        return intent;
    }

    /**
     * Sign-in activity를 띄우는 intent를 생성한다.
     */
    public static Intent createSignInIntent() {
        Intent intent = new Intent(ACTION_SIGN_IN);
        return intent;
    }

    /**
     * Save As 대화상자를 띄우는 intent.
     * @param filenameNoExt 파일명 입력란 초기값(확장자 제외). null 가능
     * @param extension 파일명 확장자 (예 'pptx'). null 가능
     * @param initialDir 초기 설정 디렉토리. null 가능
     */
    public static Intent createSaveAsIntent(String filenameNoExt, String extension, String initialDir) {
        Intent intent = new Intent("com.netffice.intent.action.SAVE_AS");
        intent.setPackage(getPackageId());
        if(filenameNoExt==null) {
            filenameNoExt = "Unnamed";
        }
        intent.putExtra("init_name", filenameNoExt);
        intent.putExtra("extension", extension);
        intent.putExtra("init_dir", initialDir);

        if(DEBUG) {
            String msg = String.format("createSaveAsIntent(%s, %s, %s) : %s", filenameNoExt, extension, initialDir, intent);
            Log.d(TAG, msg);
        }
        return intent;
    }

    public static Intent createSaveAsIntent(String filenameNoExt, String[] extensions, int selectedExtIdx, String initialDir) {
        Intent intent = new Intent("com.netffice.intent.action.SAVE_AS");
        intent.setPackage(getPackageId());
        if(filenameNoExt==null) {
            filenameNoExt = "Unnamed";
        }
        intent.putExtra("init_name", filenameNoExt);
        intent.putExtra(EXTRA_EXTENSIONS, extensions);
        intent.putExtra(EXTRA_SAVEAS_SELECTED_EXT_INDEX, selectedExtIdx);
        intent.putExtra("init_dir", initialDir);

        if(DEBUG) {
            String exts = Arrays.deepToString(extensions);
            String msg = String.format("createSaveAsIntent(%s, %s, %d, %s) : %s", filenameNoExt, exts, selectedExtIdx, initialDir, intent);
            Log.d(TAG, msg);
        }
        return intent;
    }

    /**
     * 결과는 IntentReader#readResultPathForPicker(Intent)로 읽는다.
     * @param initialDir 초기 디렉토리 경로. null 가능.
     */
    public static Intent createFilePickerIntent(File initialDir) {
        return createFilePickerIntent(initialDir, false);
    }

    /**
     * 결과는 IntentReader#readResultPathForPicker(Intent)로 읽는다.
     * @param initialDir 초기 디렉토리 경로. null 가능.
     * @param localOnly cloud는 제외하고 local file/dir만 listing.
     */
    public static Intent createFilePickerIntent(File initialDir, boolean localOnly) {
        return createFilePickerIntent(initialDir, true, !localOnly);
    }

    public static Intent createFilePickerIntent(File initialDir, boolean showLocals, boolean showClouds) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setPackage(getPackageId());

        intent.putExtra(EXTRA_INIT_DIR, initialDir==null ? null : initialDir.getPath());
        intent.putExtra(EXTRA_SHOW_LOCALS, showLocals);
        intent.putExtra(EXTRA_SHOW_CLOUDS, showClouds);
        if(DEBUG) {
            String msg = String.format("createFilePickerIntent(%s, %b, %b) - %s", initialDir, showLocals, showClouds, intent);
            Log.d(TAG, msg);
        }
        return intent;
    }

    /**
     * @param initialDir 초기에 보여줄 디렉토리. null이면 netffice app에게 맡김
     * @param showLocals local folder들을 보여줄지 여부
     * @param showClouds online folder들을 보여줄지 여부
     * @param extsToShow 표시할 확장자 목록 (예를들면 {["hwp", "hwpx", "hwt"]}). 모두 보이려면 null
     * @return
     */
    public static Intent createFilePickerIntent(File initialDir, boolean showLocals, boolean showClouds, String[] extsToShow) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setPackage(getPackageId());
        if(initialDir!=null) {
            Uri uriInitialDir = Uri.fromFile(initialDir);
            intent.setData(uriInitialDir);
        }

        intent.putExtra(EXTRA_SHOW_LOCALS, showLocals);
        intent.putExtra(EXTRA_SHOW_CLOUDS, showClouds);

        if(extsToShow!=null) {
            intent.putExtra(EXTRA_INCLUDE_EXT_LIST, extsToShow);
        }
        return intent;
    }

    /** 문서의 저장이 완료되었음을 Netffice App에 알려줌 */
    public static void notifySaveCompleted(Context cxt, String path) {
        Intent saveCompletedIntent = createSaveCompletedIntent(path);
        cxt.sendBroadcast(saveCompletedIntent);
    }

    private static Intent createSaveCompletedIntent(String path) {
        Intent saveCompletedIntent = new Intent("com.netffice.intent.action.SAVE_COMPLETED");
        saveCompletedIntent.putExtra("com.netffice.intent.extra.FILE_PATH", path);
        saveCompletedIntent.setPackage(getPackageId());
        return saveCompletedIntent;
    }

    /** File ID생성 취소. http://bts.hancom.com/show_bug.cgi?id=150121 */
    public static void undoNotifySaveCompleted(Context cxt, String path) {
        Intent saveCompletedIntent = createSaveCompletedIntent(path);
        saveCompletedIntent.putExtra(EXTRA_UNDO, true);
        cxt.sendBroadcast(saveCompletedIntent);
    }

    /** PRO계정 설명/업그레이드 UI 띄우기 */
    public static Intent createProGuideIntent() {
        Intent intent = new Intent();
        intent.setClassName(getPackageId(), "com.tf.thinkdroid.drive.paid.ProGuideActivity");
        return intent;
    }

    private void requestSessionID() {
        if(bound) {
            Message msg = Message.obtain(null, NetfficeConnect.MSG_WHAT_GET_SESSION_ID);
            msg.replyTo = inMessenger;
            try {
                service.send(msg);
            } catch (RemoteException e) {
                Log.i(TAG, "failed to request session ID", e);
            }
        } else {
            Log.i(TAG, "not bound");
            Thread.dumpStack();
        }
    }

    private static int longToDays(long timestamp) {
        return (int) (timestamp / 1000 / 60 / 60 / 24);
    }

    /** Local에 설치된 Drive App들의 package name 목록을 가져온다 */
    public static List<String> queryDrivePackages(PackageManager pm) {
        Intent intent = new Intent(ACTION_SIGN_IN);
        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        int size = resolveInfos.size();
        List<String> pkgIdList = new ArrayList<String>(size);
        for(int i=0;i<size;i++) {
            ResolveInfo rsInfo = resolveInfos.get(i);
            pkgIdList.add(rsInfo.activityInfo.packageName);
        }
        return pkgIdList;
    }

    /** 익명평가판 을사용중인가? */
    public static boolean isAnonymousEval(String sessionId) {
        return "anonymous-session".equals(sessionId);
    }
}