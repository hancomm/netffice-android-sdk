package com.netffice.clientlib;

import android.content.Intent;

/**
 * 상수값을 직접 사용하지 않기위한 유틸리티
 * Created by accent on 15. 2. 25.
 */
public class IntentReader {
    private IntentReader() {
        // static methods only
    }

    public static final String readResultPathExtra(Intent intent) {
        return intent.getStringExtra("return_path");
    }

    /**
     * Directory Chooser와는 다르게 file picker는 android표준 ACTION_PICK을 사용하고 결과도 extra가 아닌 Uri에서 읽어옴
     * @return null이 나오는 경우도 있다
     */
    public static final String readResultPathForPicker(Intent intent) {
        String result = null;
        try {
            result = intent.getData().getPath();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /** 호출한 drive app의 package ID를 반환. 실패하면 null */
    public static final String readDriveApp(Intent intent) {
        if(intent==null) return null;
        String pkgId = intent.getStringExtra("com.netffice24.intent.extra.DRIVE");
        return pkgId;
    }
}
