[TOC]

개요
===
- `Netffice24 app(혹은 clone Drive app)` <=> `외부앱`간의 연동 지원

내용물
===
- [client-lib.jar](http://devref.netffice24.com/dist/client-lib.jar) with [Javadoc](http://devref.netffice24.com/android-javadoc/) (Client app에 dependency로 추가해야 함)
- [sample-app-debug.apk](http://devref.netffice24.com/dist/sample-app-debug.apk) (client-lib 연동 예제)
![netfficepal-1.png](https://bitbucket.org/repo/bjAEnr/images/3417122253-netfficepal-1.png)

기능
===
* 로컬에 설치된 Drive app 목록 query ([NetfficeConnect.queryDrivePackages()](http://devref.netffice24.com/android-javadoc/com/netffice/clientlib/NetfficeConnect.html#queryDrivePackages-PackageManager-))
    * [예제소스](https://bitbucket.org/hancomm/netffice-android-sdk/src/431e6ae38317c977e823917753b277fcca03f275/sample-app/src/main/java/com/netffice/example/ExampleActivity.java?at=master&fileviewer=file-view-default#ExampleActivity.java-124)
* 호출된 client app에서 drive app 정보를 얻어오기 ([IntentReader.readDriveApp()](http://devref.netffice24.com/android-javadoc/com/netffice/clientlib/IntentReader.html#readDriveApp-Intent-))
    * [예제소스](https://bitbucket.org/hancomm/netffice-android-sdk/src/431e6ae38317c977e823917753b277fcca03f275/sample-app/src/main/java/com/netffice/example/MockXlsxViewerActivity.java?at=master&fileviewer=file-view-default#MockXlsxViewerActivity.java-20)
- Sign in(up) activity 호출 ([NetfficeConnect.createSignInIntent()](http://devref.netffice24.com/android-javadoc/com/netffice/clientlib/NetfficeConnect.html#createSignInIntent--))
* Netffice App download용 intent 생성기
* File [Open](http://devref.netffice24.com/android-javadoc/com/netffice/clientlib/NetfficeConnect.html#createFilePickerIntent-java.io.File-boolean-boolean-java.lang.String:A-)/[Save](http://devref.netffice24.com/android-javadoc/com/netffice/clientlib/NetfficeConnect.html#createSaveAsIntent-java.lang.String-java.lang.String-java.lang.String-) activity 호출 (Local + Online)
* 기타

관련링크
===
- [개발가이드 문서](https://docs.google.com/presentation/d/1aCNfNYbv1GyBQgJMr57jcVF5L_9rp7hYrWMkBfJgtxI/edit?usp=sharing)