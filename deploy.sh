#!/usr/bin/env bash

set -e

AWS_PROFILE=netffice24
NAME=netffice-android-sdk

# aws configure --profile netffice24

echo "================================================================"
echo "[$NAME] Build..."
echo "================================================================"
./gradlew build noTimestampJavadoc

echo "================================================================"
echo "[$NAME] Deploy..."
echo "[$NAME] AWS Profile: " $AWS_PROFILE
echo "================================================================"

aws --profile $AWS_PROFILE s3 cp client-lib/build/libs/client-lib.jar s3://devref.netffice24.com/dist/
aws --profile $AWS_PROFILE s3 cp sample-app/build/outputs/apk/sample-app-debug.apk s3://devref.netffice24.com/dist/
aws --profile $AWS_PROFILE s3 cp client-lib/build/docs/javadoc s3://devref.netffice24.com/android-javadoc --recursive

echo "================================================================"
echo "#  ___                   _ _ _   "
echo "# / __|_  _ __ ___ _____| | | |  "
echo "# \__ \ || / _/ -_|_-<_-<_|_|_|  "
echo "# |___/\_,_\__\___/__/__(_|_|_)  "
echo "#                                "
echo "================================================================"
